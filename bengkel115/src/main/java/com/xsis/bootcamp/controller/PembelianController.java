package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.PembelianBarang;
import com.xsis.bootcamp.model.SparepartModel;
import com.xsis.bootcamp.service.PemasokService;
import com.xsis.bootcamp.service.PembelianService;
import com.xsis.bootcamp.service.SparepartService;

@Controller
public class PembelianController extends BaseController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private PembelianService pembelianService;

	@Autowired
	private PemasokService pemasokService;
	
	@Autowired
	private SparepartService sparepartService;
	
	private List<PembelianBarang> pembelianList = new ArrayList<PembelianBarang>();
	private List<PemasokModel> pemasokList = new ArrayList<PemasokModel>();
	private List<SparepartModel> sparepartList = new ArrayList<SparepartModel>();

	@RequestMapping(value="/transaksi/txpembelian")
	public ModelAndView index(Model model){
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/transaksi/txpembelian");
	}
	
	@RequestMapping(value="/transaksi/pembelian/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception{
		pembelianList = this.pembelianService.get();
		model.addAttribute("pembelianList", pembelianList);
		return new ModelAndView("/transaksi/pembelian/list");
	}
	
	
	@RequestMapping(value = "/transaksi/pembelian/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) throws Exception {
		pemasokList = this.pemasokService.get();
		model.addAttribute("pemasokList", pemasokList);
		return new ModelAndView("/transaksi/pembelian/add");
	}
	
	@RequestMapping(value = "/transaksi/pembelian/addDetail", method = RequestMethod.GET)
	public ModelAndView addDetail(Model model) throws Exception {
		sparepartList = this.sparepartService.get();
		model.addAttribute("sparepartList", sparepartList);
		return new ModelAndView("/transaksi/pembelian/addDetail");
	}
	
	@RequestMapping(value = "/transaksi/pembelian/save")
	public String save(Model model, @ModelAttribute PembelianBarang item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		PembelianBarang tmp = null;
		try {
			tmp = this.pembelianService.getById(item.getID());
			if (action.equals("insert")) {
				this.pembelianService.insert(item);
			} else if(action.equals("update")) {
				this.pembelianService.update(item);
			} else if (action.equals("delete")) {
				this.pembelianService.delete(item);
			} else {
			
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/transaksi/pembelian/save";
	}
	
	@RequestMapping(value = "/transaksi/pembelian/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PembelianBarang result = new PembelianBarang();
		try {
			result = this.pembelianService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/transaksi/pembelian/detail");
	}
	
	@RequestMapping(value = "/transaksi/pembelian/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PembelianBarang result = new PembelianBarang();
		try {
			result = this.pembelianService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/transaksi/pembelian/edit");
	}
	
	@RequestMapping(value = "/transaksi/pembelian/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PembelianBarang result = new PembelianBarang();
		try {
			result = this.pembelianService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/transaksi/pembelian/delete");
	}
}
