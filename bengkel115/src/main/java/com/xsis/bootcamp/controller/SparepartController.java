package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.SparepartModel;
import com.xsis.bootcamp.service.KategoriService;
import com.xsis.bootcamp.service.SparepartService;

@Controller
public class SparepartController extends BaseController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private SparepartService service;
	
	@Autowired 
	private KategoriService kategoriService;

	private List<SparepartModel> sparepartList = new ArrayList<SparepartModel>();
	
	private List<KategoriModel> kategoriList = new ArrayList<KategoriModel>();

	@RequestMapping(value = "/master/sparepart")
	public ModelAndView SparepartIndex(Model model) {
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/master/sparepart");
	}

	@RequestMapping(value = "/master/sparepart/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception {
		sparepartList = this.service.get();
		model.addAttribute("sparepartList", sparepartList);
		return new ModelAndView("/master/branch/list");
	}
	
	@RequestMapping(value = "/master/sparepart/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) throws Exception {
		kategoriList = this.kategoriService.get();
		model.addAttribute("kategoriList", kategoriList);
		return new ModelAndView("/master/sparepart/add");
	}
	
	@RequestMapping(value = "/master/sparepart/save")
	public String save(Model model, @ModelAttribute SparepartModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		SparepartModel tmp = null;
		try {
			tmp = this.service.getById(item.getId());

			if (action.equals("insert")) {
				this.service.insert(item);
			} else if(action.equals("update")) {
				this.service.update(item);
			} else if (action.equals("delete")) {
				this.service.delete(item);
			} else {
			
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/sparepart/save";
	}
	
	@RequestMapping(value = "/master/sparepart/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		SparepartModel result = new SparepartModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);
		return new ModelAndView("/master/sparepart/detail");
	}
	
	@RequestMapping(value = "/master/sparepart/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		SparepartModel result = new SparepartModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/sparepart/edit");
	}
	
	@RequestMapping(value = "/master/sparepart/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		SparepartModel result = new SparepartModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/sparepart/delete");
	}
	
	@RequestMapping(value = "/master/sparepart/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) {
		String keySearch = request.getParameter("key");
		List<SparepartModel> result = null;
		try {
			result = this.service.search(keySearch);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("sparepartList", result);

		return new ModelAndView("/master/sparepart/list");
	}
}
