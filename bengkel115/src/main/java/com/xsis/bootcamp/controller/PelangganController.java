package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.service.PelangganService;

@Controller
public class PelangganController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PelangganService pelangganService;
	
	private List<PelangganModel> pelangganList = new ArrayList<PelangganModel>();

	@RequestMapping(value = "/master/pelanggan")
	public ModelAndView index(Model model) {
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/master/pelanggan");
	}

	@RequestMapping(value = "/master/pelanggan/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception {
		
		pelangganList = this.pelangganService.get();
		model.addAttribute("pelangganList",pelangganList);

		return new ModelAndView("/master/pelanggan/list");
	}
	
	@RequestMapping(value = "/master/pelanggan/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		return new ModelAndView("/master/pelanggan/add");
	}
	
	@RequestMapping(value = "/master/pelanggan/save")
	public String save(Model model, @ModelAttribute PelangganModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		PelangganModel tmp = null;
		try {
			tmp = this.pelangganService.getById(item.getId());

			if (action.equals("insert")) {
				this.pelangganService.insert(item);
			} else if(action.equals("update")) {
				this.pelangganService.update(item);
			} else if (action.equals("delete")) {
				this.pelangganService.delete(tmp);
			} else {
			
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/pelanggan/save";
	}
	
	@RequestMapping(value = "/master/pelanggan/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PelangganModel result = new PelangganModel();
		try {
			result = this.pelangganService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/pelanggan/detail");
	}
	
	@RequestMapping(value = "/master/pelanggan/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PelangganModel result = new PelangganModel();
		try {
			result = this.pelangganService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/pelanggan/edit");
	}
	
	@RequestMapping(value = "/master/pelanggan/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PelangganModel result = new PelangganModel();
		try {
			result = this.pelangganService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/pelanggan/delete");
	}
	
	@RequestMapping(value = "/master/pelanggan/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) {
		String keySearch = request.getParameter("key");
		List<PelangganModel> result = null;
		try {
			result = this.pelangganService.search(keySearch);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("pelangganList", result);

		return new ModelAndView("/master/pelanggan/list");
	}

}
