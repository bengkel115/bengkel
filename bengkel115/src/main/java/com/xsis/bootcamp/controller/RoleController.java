package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.bootcamp.model.RoleModel;
import com.xsis.bootcamp.service.RoleService;

@Controller
public class RoleController extends BaseController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private RoleService roleService;
	
	private List<RoleModel> roleList = new ArrayList<RoleModel>();
	
	@RequestMapping(value = "/master/role")
	public ModelAndView index(Model model) {
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/master/role");
	}
	
	@RequestMapping(value = "/master/role/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception {
		roleList = this.roleService.get();
		model.addAttribute("roleList", roleList);

		return new ModelAndView("/master/role/list");
	}
	
	@RequestMapping(value = "/master/role/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		return new ModelAndView("/master/role/add");
	}
	
	@RequestMapping(value = "/master/role/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		RoleModel result = new RoleModel();
		try {
			result = this.roleService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("role", result);

		return new ModelAndView("/master/role/detail");
	}
	
	@RequestMapping(value = "/master/role/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		RoleModel result = new RoleModel();
		try {
			result = this.roleService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("role", result);

		return new ModelAndView("/master/role/edit");
	}
	
	@RequestMapping(value = "/master/role/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		RoleModel result = new RoleModel();
		try {
			result = this.roleService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("role", result);

		return new ModelAndView("/master/role/delete");
	}
	
	@RequestMapping(value = "/master/role/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) {
		String keySearch = request.getParameter("key");
		List<RoleModel> result = null;
		try {
			result = this.roleService.search(keySearch);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("roleList", result);

		return new ModelAndView("/master/role/list");
	}
	
	@RequestMapping(value = "/master/role/save")
	public String save(Model model, @ModelAttribute RoleModel role, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		RoleModel tmp = null;
		try {
			tmp = this.roleService.getById(role.getId());

			if (action.equals("insert")) {
				this.roleService.insert(role);
			}else if (action.equals("update")){
				this.roleService.update(role);
			}else if (action.equals("delete")){
				this.roleService.delete(tmp);
			}else{
				
			}

			result = "success";
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/role/save";
	}
}
