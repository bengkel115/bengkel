package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.bootcamp.model.KaryawanModel;
import com.xsis.bootcamp.service.KaryawanService;

@Controller
public class KaryawanController extends BaseController {
	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private KaryawanService karyawanService;
	
	private List<KaryawanModel> karyawanList = new ArrayList<KaryawanModel>();
	
	@RequestMapping(value = "/master/karyawan")
	public ModelAndView index(Model model) {
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/master/karyawan");
	}
	
	@RequestMapping(value = "/master/karyawan/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception {
		karyawanList = this.karyawanService.get();
		model.addAttribute("karyawanList", karyawanList);

		return new ModelAndView("/master/karyawan/list");
	}
	
	@RequestMapping(value = "/master/karyawan/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		return new ModelAndView("/master/karyawan/add");
	}
	
	@RequestMapping(value = "/master/karyawan/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		KaryawanModel result = new KaryawanModel();
		try {
			result = this.karyawanService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("karyawan", result);

		return new ModelAndView("/master/karyawan/detail");
	}
	
	@RequestMapping(value = "/master/karyawan/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		KaryawanModel result = new KaryawanModel();
		try {
			result = this.karyawanService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("karyawan", result);

		return new ModelAndView("/master/karyawan/edit");
	}
	
	@RequestMapping(value = "/master/karyawan/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		KaryawanModel result = new KaryawanModel();
		try {
			result = this.karyawanService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("karyawan", result);

		return new ModelAndView("/master/karyawan/delete");
	}
	
	@RequestMapping(value = "/master/karyawan/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) {
		String keySearch = request.getParameter("key");
		List<KaryawanModel> result = null;
		try {
			result = this.karyawanService.search(keySearch);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("karyawanList", result);

		return new ModelAndView("/master/karyawan/list");
	}
	
	@RequestMapping(value = "/master/karyawan/save")
	public String save(Model model, @ModelAttribute KaryawanModel karyawan, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		KaryawanModel tmp = null;
		try {
			tmp = this.karyawanService.getById(karyawan.getId());

			if (action.equals("insert")) {
				this.karyawanService.insert(karyawan);
			}else if (action.equals("update")){
				this.karyawanService.update(karyawan);
			}else if (action.equals("delete")){
				this.karyawanService.delete(tmp);
			}else{
				
			}

			result = "success";
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/karyawan/save";
	}
}
