package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.xsis.bootcamp.model.KaryawanModel;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.service.KategoriService;

@Controller
public class KategoriController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private KategoriService kategoriService;
	
	
	private List<KategoriModel> kategoriList = new ArrayList<KategoriModel>();

	@RequestMapping(value = "/master/kategori")
	public ModelAndView index(Model model) {		
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/master/kategori");
	}

	@RequestMapping(value = "/master/kategori/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception {
		
		kategoriList = this.kategoriService.get();
		model.addAttribute("kategoriList",kategoriList);

		return new ModelAndView("/master/kategori/list");
	}
	
	@RequestMapping(value = "/master/kategori/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		return new ModelAndView("/master/kategori/add");
	}
	
	@RequestMapping(value = "/master/kategori/save")
	public String save(Model model, @ModelAttribute KategoriModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		KategoriModel tmp = null;
		try {
			tmp = this.kategoriService.getById(item.getId());

			if (action.equals("insert")) {
				this.kategoriService.insert(item);
			} else if(action.equals("update")) {
				this.kategoriService.update(item);
			} else if (action.equals("delete")) {
				this.kategoriService.delete(tmp);
			} else {
			
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/kategori/save";
	}
	
	@RequestMapping(value = "/master/kategori/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		KategoriModel result = new KategoriModel();
		try {
			result = this.kategoriService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/kategori/detail");
	}
	
	@RequestMapping(value = "/master/kategori/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		KategoriModel result = new KategoriModel();
		try {
			result = this.kategoriService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/kategori/edit");
	}
	
	@RequestMapping(value = "/master/kategori/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		KategoriModel result = new KategoriModel();
		try {
			result = this.kategoriService.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/kategori/delete");
	}
	
	@RequestMapping(value = "/master/kategori/search", method = RequestMethod.GET)
	public ModelAndView search(Model model, HttpServletRequest request) {
		String keySearch = request.getParameter("key");
		List<KategoriModel> result = null;
		try {
			result = this.kategoriService.search(keySearch);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("kategoriList", result);

		return new ModelAndView("/master/kategori/list");
	}

}
