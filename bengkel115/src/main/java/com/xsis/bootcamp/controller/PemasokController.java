package com.xsis.bootcamp.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.service.PemasokService;

@Controller
public class PemasokController extends BaseController {
	private Log log = LogFactory.getLog(getClass());

	@Autowired
	private PemasokService service;

	private List<PemasokModel> pemasokList = new ArrayList<PemasokModel>();

	@RequestMapping(value = "/master/pemasok")
	public ModelAndView PemasokIndex(Model model) {
		model.addAttribute("userName", this.getUserName());
		return new ModelAndView("/master/pemasok");
	}

	@RequestMapping(value = "/master/pemasok/list", method = RequestMethod.GET)
	public ModelAndView list(Model model) throws Exception {
		pemasokList = this.service.get();
		model.addAttribute("pemasokList", pemasokList);
		return new ModelAndView("/master/pemasok/list");
	}
	
	@RequestMapping(value = "/master/pemasok/add", method = RequestMethod.GET)
	public ModelAndView add(Model model) {
		return new ModelAndView("/master/pemasok/add");
	}
	
	@RequestMapping(value = "/master/pemasok/save")
	public String save(Model model, @ModelAttribute PemasokModel item, HttpServletRequest request) {
		String result = "";
		String action = request.getParameter("action");
		PemasokModel tmp = null;
		try {
			tmp = this.service.getById(item.getId());

			if (action.equals("insert")) {
				this.service.insert(item);
			} else if(action.equals("update")) {
				this.service.update(item);
			} else if (action.equals("delete")) {
				this.service.delete(item);
			} else {
			
			}
			result = "success";

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result = "failed";
		}

		model.addAttribute("message", result);

		return "/master/pemasok/save";
	}
	
	@RequestMapping(value = "/master/pemasok/detail")
	public ModelAndView detail(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PemasokModel result = new PemasokModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/pemasok/detail");
	}
	
	@RequestMapping(value = "/master/pemasok/edit")
	public ModelAndView edit(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PemasokModel result = new PemasokModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/pemasok/edit");
	}
	
	@RequestMapping(value = "/master/pemasok/delete")
	public ModelAndView delete(Model model, HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id"));
		PemasokModel result = new PemasokModel();
		try {
			result = this.service.getById(id);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		model.addAttribute("item", result);

		return new ModelAndView("/master/pemasok/delete");
	}
}
