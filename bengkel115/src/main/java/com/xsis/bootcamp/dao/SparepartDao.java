package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.SparepartModel;

public interface SparepartDao {
	public List<SparepartModel> get() throws Exception;
	public SparepartModel getById(int id) throws Exception;
	public void insert(SparepartModel model) throws Exception;
	public void update(SparepartModel model) throws Exception;
	public void delete(SparepartModel model) throws Exception;
	public List<SparepartModel> search(String keySearch) throws Exception;
}
