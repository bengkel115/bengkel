package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.Criteria;
import org.hibernate.Session;
import com.xsis.bootcamp.dao.SparepartDao;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.SparepartModel;

@Repository
public class SparepartDaoImpl implements SparepartDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<SparepartModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<SparepartModel> result = session.createQuery("from SparepartModel").list();
		return result;
	}

	@Override
	public SparepartModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		SparepartModel result = session.get(SparepartModel.class, id);
		return result;
	}

	@Override
	public void insert(SparepartModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);

	}

	@Override
	public void update(SparepartModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);

	}

	@Override
	public void delete(SparepartModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public List<SparepartModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(SparepartModel.class);
		criteria.add(Restrictions.like("namaSparepart", "%" + keySearch + "%"));
		List<SparepartModel> result = criteria.list();
		return result;
	}

}
