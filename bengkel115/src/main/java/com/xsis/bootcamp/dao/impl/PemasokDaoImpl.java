package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PemasokDao;
import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.SparepartModel;

@Repository
public class PemasokDaoImpl implements PemasokDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<PemasokModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<PemasokModel> result = session.createQuery("from PemasokModel").list();
		return result;
	}

	@Override
	public PemasokModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		PemasokModel result = session.get(PemasokModel.class, id);
		return result;
	}

	@Override
	public void insert(PemasokModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
		
	}
	
	@Override
	public void update(PemasokModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
		
	}
	

	@Override
	public void delete(PemasokModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

}
