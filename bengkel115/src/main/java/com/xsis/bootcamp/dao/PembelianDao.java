package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.PembelianBarang;

public interface PembelianDao {
	public List<PembelianBarang> get() throws Exception;	
	public PembelianBarang getById(int id) throws Exception;
	public void insert(PembelianBarang model) throws Exception;
	public void update(PembelianBarang model) throws Exception;
	public void delete(PembelianBarang model) throws Exception;
}
