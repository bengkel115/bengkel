package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PelangganDao;
import com.xsis.bootcamp.model.PelangganModel;

@Repository
public class PelangganDaoImpl implements PelangganDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<PelangganModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<PelangganModel> result = session.createQuery("from PelangganModel").list();
		return result;
	}

	@Override
	public PelangganModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		PelangganModel result = session.get(PelangganModel.class, id);
		return result;
	}

	@Override
	public void insert(PelangganModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
		
	}

	@Override
	public void update(PelangganModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
		
	}

	@Override
	public void delete(PelangganModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public List<PelangganModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(PelangganModel.class);
		criteria.add(Restrictions.like("namaPelanggan", "%" + keySearch + "%"));
		List<PelangganModel> result = criteria.list();
		return result;
	}


	
}
