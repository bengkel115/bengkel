package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.PembelianDao;
import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.PembelianBarang;

@Repository
public class PembelianDaoImpl implements PembelianDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<PembelianBarang> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<PembelianBarang> result = session.createQuery("from PembelianBarang").list();
		return result;
	}
	@Override
	public PembelianBarang getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		PembelianBarang result = session.get(PembelianBarang.class, id);
		return result;
	}

	@Override
	public void insert(PembelianBarang model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
		
	}
	
	@Override
	public void update(PembelianBarang model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
		
	}
	

	@Override
	public void delete(PembelianBarang model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

}
