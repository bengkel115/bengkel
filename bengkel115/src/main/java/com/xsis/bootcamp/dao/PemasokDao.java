package com.xsis.bootcamp.dao;

import java.util.List;

import com.xsis.bootcamp.model.PemasokModel;

public interface PemasokDao {
	public List<PemasokModel> get() throws Exception;
	public PemasokModel getById(int id) throws Exception;
	public void insert(PemasokModel model) throws Exception;
	public void update(PemasokModel model) throws Exception;
	public void delete(PemasokModel model) throws Exception;
}
