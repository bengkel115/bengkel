package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.KategoriDao;
import com.xsis.bootcamp.model.KategoriModel;

@Repository
public class KategoriDaoImpl implements KategoriDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<KategoriModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<KategoriModel> result = session.createQuery("from KategoriModel").list();
		return result;
	}

	@Override
	public KategoriModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		KategoriModel result = session.get(KategoriModel.class, id);
		return result;
	}

	@Override
	public void insert(KategoriModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
		
	}

	@Override
	public void update(KategoriModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
		
	}

	@Override
	public void delete(KategoriModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public List<KategoriModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(KategoriModel.class);
		criteria.add(Restrictions.like("namaKategori", "%" + keySearch + "%"));
		List<KategoriModel> result = criteria.list();
		return result;
	}


	
}
