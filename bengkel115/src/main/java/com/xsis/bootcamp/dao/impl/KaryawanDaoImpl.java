package com.xsis.bootcamp.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.bootcamp.dao.KaryawanDao;
import com.xsis.bootcamp.model.KaryawanModel;

@Repository
public class KaryawanDaoImpl implements KaryawanDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<KaryawanModel> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<KaryawanModel> result = session.createQuery("from KaryawanModel").list();
		return result;
	}

	@Override
	public void insert(KaryawanModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(model);
	}

	@Override
	public KaryawanModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		KaryawanModel result = session.get(KaryawanModel.class, id);
		return result;
	}

	@Override
	public void update(KaryawanModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(model);
	}

	@Override
	public void delete(KaryawanModel model) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(model);
	}

	@Override
	public List<KaryawanModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(KaryawanModel.class);
		criteria.add(Restrictions.like("namaKaryawan", "%" + keySearch + "%"));
		List<KaryawanModel> result = criteria.list();
		return result;
	}

}
