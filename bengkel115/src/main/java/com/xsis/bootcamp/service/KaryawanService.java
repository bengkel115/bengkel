package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.KaryawanModel;

public interface KaryawanService {
	
	public List<KaryawanModel> get() throws Exception;
	
	public void insert(KaryawanModel model) throws Exception;
	
	public KaryawanModel getById(int id) throws Exception;
	
	public void update(KaryawanModel model) throws Exception;
	
	public void delete(KaryawanModel model) throws Exception;
	
	public List<KaryawanModel> search(String keySearch) throws Exception;
}
