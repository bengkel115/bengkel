package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.PelangganModel;

public interface PelangganService {
	public List<PelangganModel> get() throws Exception;
	public PelangganModel getById(int id) throws Exception;
	public void insert(PelangganModel model) throws Exception;
	public void update(PelangganModel model) throws Exception;
	public void delete(PelangganModel model) throws Exception;
	public List<PelangganModel> search(String keySearch) throws Exception;

}
