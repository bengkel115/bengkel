package com.xsis.bootcamp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.KategoriDao;
import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.service.KategoriService;

@Service
@Transactional
public class KategoriServiceImpl implements KategoriService {
	
	@Autowired private KategoriDao kategoriDao ;

	@Override
	public List<KategoriModel> get() throws Exception {
		// TODO Auto-generated method stub
		return kategoriDao.get();
	}

	@Override
	public KategoriModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.kategoriDao.getById(id);
	}

	@Override
	public void insert(KategoriModel model) throws Exception {
		// TODO Auto-generated method stub
		this.kategoriDao.insert(model);
		
	}

	@Override
	public void update(KategoriModel model) throws Exception {
		// TODO Auto-generated method stub
		this.kategoriDao.update(model);
	}

	@Override
	public void delete(KategoriModel model) throws Exception {
		// TODO Auto-generated method stub
		this.kategoriDao.delete(model);
	}

	@Override
	public List<KategoriModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		return this.kategoriDao.search(keySearch);
	}


}
