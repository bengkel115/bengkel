package com.xsis.bootcamp.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PelangganDao;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.service.PelangganService;

@Service
@Transactional
public class PelangganServiceImpl implements PelangganService {
	
	@Autowired private PelangganDao pelangganDao ;

	@Override
	public List<PelangganModel> get() throws Exception {
		// TODO Auto-generated method stub
		return pelangganDao.get();
	}

	@Override
	public PelangganModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.pelangganDao.getById(id);
	}

	@Override
	public void insert(PelangganModel model) throws Exception {
		// TODO Auto-generated method stub
		this.pelangganDao.insert(model);
		
	}

	@Override
	public void update(PelangganModel model) throws Exception {
		// TODO Auto-generated method stub
		this.pelangganDao.update(model);
	}

	@Override
	public void delete(PelangganModel model) throws Exception {
		// TODO Auto-generated method stub
		this.pelangganDao.delete(model);
	}

	@Override
	public List<PelangganModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		return this.pelangganDao.search(keySearch);
	}


}
