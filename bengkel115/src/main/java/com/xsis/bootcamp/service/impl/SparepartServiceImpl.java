package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.SparepartDao;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.SparepartModel;
import com.xsis.bootcamp.service.SparepartService;

@Service
@Transactional
public class SparepartServiceImpl implements SparepartService{

	@Autowired
	private SparepartDao sparepartDao;
	
	@Override
	public List<SparepartModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.sparepartDao.get();
	}

	@Override
	public SparepartModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.sparepartDao.getById(id);
	}

	@Override
	public void insert(SparepartModel model) throws Exception {
		// TODO Auto-generated method stub
		this.sparepartDao.insert(model);
		
	}
	
	@Override
	public void update(SparepartModel model) throws Exception {
		// TODO Auto-generated method stub
		this.sparepartDao.update(model);
	}
	
	@Override
	public void delete(SparepartModel model) throws Exception {
		// TODO Auto-generated method stub
		this.sparepartDao.delete(model);
	}

	@Override
	public List<SparepartModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		return this.sparepartDao.search(keySearch);
	}

}
