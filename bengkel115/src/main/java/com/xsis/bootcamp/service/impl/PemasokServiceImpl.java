package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PemasokDao;
import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.SparepartModel;
import com.xsis.bootcamp.service.PemasokService;

@Service
@Transactional
public class PemasokServiceImpl implements PemasokService{
	@Autowired 
	private PemasokDao pemasokDao;

	@Override
	public List<PemasokModel> get() throws Exception {
		// TODO Auto-generated method stub
		return this.pemasokDao.get();
	}

	@Override
	public PemasokModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.pemasokDao.getById(id);
	}

	@Override
	public void insert(PemasokModel model) throws Exception {
		// TODO Auto-generated method stub
		this.pemasokDao.insert(model);
		
	}
	
	@Override
	public void update(PemasokModel model) throws Exception {
		// TODO Auto-generated method stub
		this.pemasokDao.update(model);
	}
	
	@Override
	public void delete(PemasokModel model) throws Exception {
		// TODO Auto-generated method stub
		this.pemasokDao.delete(model);
	}

}
