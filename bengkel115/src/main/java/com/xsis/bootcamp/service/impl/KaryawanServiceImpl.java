package com.xsis.bootcamp.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.bootcamp.dao.KaryawanDao;
import com.xsis.bootcamp.model.KaryawanModel;
import com.xsis.bootcamp.service.KaryawanService;
@Service
@Transactional
public class KaryawanServiceImpl implements KaryawanService{
	@Autowired
	private KaryawanDao karyawanDao;

	@Override
	public List<KaryawanModel> get() throws Exception {
		// TODO Auto-generated method stub
		return karyawanDao.get();
	}

	@Override
	public void insert(KaryawanModel model) throws Exception {
		// TODO Auto-generated method stub
		this.karyawanDao.insert(model);
	}

	@Override
	public KaryawanModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.karyawanDao.getById(id);
	}

	@Override
	public void update(KaryawanModel model) throws Exception {
		// TODO Auto-generated method stub
		this.karyawanDao.update(model);
	}

	@Override
	public void delete(KaryawanModel model) throws Exception {
		// TODO Auto-generated method stub
		this.karyawanDao.delete(model);
	}

	@Override
	public List<KaryawanModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		return this.karyawanDao.search(keySearch);
	}

}
