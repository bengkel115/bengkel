package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.KategoriModel;

public interface KategoriService {
	public List<KategoriModel> get() throws Exception;
	public KategoriModel getById(int id) throws Exception;
	public void insert(KategoriModel model) throws Exception;
	public void update(KategoriModel model) throws Exception;
	public void delete(KategoriModel model) throws Exception;
	public List<KategoriModel> search(String keySearch) throws Exception;
}
