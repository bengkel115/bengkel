package com.xsis.bootcamp.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xsis.bootcamp.dao.RoleDao;
import com.xsis.bootcamp.model.RoleModel;
import com.xsis.bootcamp.service.RoleService;
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<RoleModel> get() throws Exception {
		// TODO Auto-generated method stub
		return roleDao.get();
	}

	@Override
	public void insert(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.roleDao.insert(model);
	}

	@Override
	public RoleModel getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.roleDao.getById(id);
	}

	@Override
	public void update(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.roleDao.update(model);
	}

	@Override
	public void delete(RoleModel model) throws Exception {
		// TODO Auto-generated method stub
		this.roleDao.delete(model);
	}

	@Override
	public List<RoleModel> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		return this.roleDao.search(keySearch);
	}
	
}
