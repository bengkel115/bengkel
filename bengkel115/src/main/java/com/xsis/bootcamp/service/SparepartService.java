package com.xsis.bootcamp.service;

import java.util.List;

import com.xsis.bootcamp.model.KategoriModel;
import com.xsis.bootcamp.model.PelangganModel;
import com.xsis.bootcamp.model.SparepartModel;

public interface SparepartService {
	public List<SparepartModel> get() throws Exception;
	public SparepartModel getById(int id) throws Exception;
	public void insert(SparepartModel model) throws Exception;
	public void update(SparepartModel model) throws Exception;
	public void delete(SparepartModel model) throws Exception;
	public List<SparepartModel> search(String keySearch) throws Exception;
}
