package com.xsis.bootcamp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.bootcamp.dao.PemasokDao;
import com.xsis.bootcamp.dao.PembelianDao;
import com.xsis.bootcamp.model.PemasokModel;
import com.xsis.bootcamp.model.PembelianBarang;
import com.xsis.bootcamp.service.PembelianService;

@Service
@Transactional
public class PembelianServiceImpl implements PembelianService{

	@Autowired 
	private PembelianDao pembelianDao;

	@Override
	public List<PembelianBarang> get() throws Exception {
		// TODO Auto-generated method stub
		return this.pembelianDao.get();
	}
	@Override
	public PembelianBarang getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return this.pembelianDao.getById(id);
	}

	@Override
	public void insert(PembelianBarang model) throws Exception {
		// TODO Auto-generated method stub
		this.pembelianDao.insert(model);
		
	}
	
	@Override
	public void update(PembelianBarang model) throws Exception {
		// TODO Auto-generated method stub
		this.pembelianDao.update(model);
	}
	
	@Override
	public void delete(PembelianBarang model) throws Exception {
		// TODO Auto-generated method stub
		this.pembelianDao.delete(model);
	}

}
