package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="detail_pembelian")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="idDetailPembelian")
public class DetailPembelian {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="detail_pembelian")
	@TableGenerator(name="detail_pembelian",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="detail_pembelian", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	private int id;
	
	@Column(name="harga")
	private String hargaitem;
	
	@Column(name="jumlah")
	private String jumlahitem;
	
	@Column(name="ID_SPAREPART")
	private int idSparepart;
	
	@OneToMany (mappedBy="detailPembelian", fetch= FetchType.EAGER)
	@JsonManagedReference
	private List<SparepartModel> sparepartModel;	
	
	@ManyToOne
	private PembelianBarang pembelian;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHargaitem() {
		return hargaitem;
	}
	public void setHargaitem(String hargaitem) {
		this.hargaitem = hargaitem;
	}
	public String getJumlahitem() {
		return jumlahitem;
	}
	public void setJumlahitem(String jumlahitem) {
		this.jumlahitem = jumlahitem;
	}
	public int getIdSparepart() {
		return idSparepart;
	}
	public void setIdSparepart(int idSparepart) {
		this.idSparepart = idSparepart;
	}
	public PembelianBarang getPembelian() {
		return pembelian;
	}
	public void setPembelian(PembelianBarang pembelian) {
		this.pembelian = pembelian;
	}
	
}
