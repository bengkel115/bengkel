package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="ROLE")
public class RoleModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="ROLE")
	@TableGenerator(name="ROLE",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="ROLE", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	private int id;
	@Column(name="KODE")
	private String kodeRole;
	@Column(name="NAMA")
	private String namaRole;
	@Column(name="KETERANGAN")
	private String keteranganRole;
	@Column(name="STATUS")
	private String statusRole;
	//private List<KaryawanModel> karyawan;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKodeRole() {
		return kodeRole;
	}
	public void setKodeRole(String kodeRole) {
		this.kodeRole = kodeRole;
	}
	public String getNamaRole() {
		return namaRole;
	}
	public void setNamaRole(String namaRole) {
		this.namaRole = namaRole;
	}
	public String getKeteranganRole() {
		return keteranganRole;
	}
	public void setKeteranganRole(String keteranganRole) {
		this.keteranganRole = keteranganRole;
	}
	public String getStatusRole() {
		return statusRole;
	}
	public void setStatusRole(String statusRole) {
		this.statusRole = statusRole;
	}
	
	/*@OneToMany(fetch=FetchType.EAGER, mappedBy="role")
	@JsonBackReference
	public List<KaryawanModel> getRoleKaryawan() {
		return karyawan;
	}
	public void setRoleKaryawan(List<KaryawanModel> karyawan) {
		this.karyawan = karyawan;
	}*/
	
}
