package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="KARYAWAN")
public class KaryawanModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="KARYAWAN")
	@TableGenerator(name="KARYAWAN",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="KARYAWAN", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	private int id;
	@Column(name="USERNAME")
	private String username;
	@Column(name="PASSWORD")
	private String password;
	@Column(name="KODE")
	private String kodeKaryawan;
	@Column(name="NAMA")
	private String namaKaryawan;
	@Column(name="TELEPON")
	private String teleponKaryawan;
	@Column(name="ALAMAT")
	private String alamatKaryawan;
	@Column(name="STATUS")
	private String statusKaryawan;
	/*@Column(name="ROLE")
	private RoleModel roleKaryawan;*/
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getKodeKaryawan() {
		return kodeKaryawan;
	}
	public void setKodeKaryawan(String kodeKaryawan) {
		this.kodeKaryawan = kodeKaryawan;
	}
	public String getNamaKaryawan() {
		return namaKaryawan;
	}
	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}
	public String getTeleponKaryawan() {
		return teleponKaryawan;
	}
	public void setTeleponKaryawan(String teleponKaryawan) {
		this.teleponKaryawan = teleponKaryawan;
	}
	public String getAlamatKaryawan() {
		return alamatKaryawan;
	}
	public void setAlamatKaryawan(String alamatKaryawan) {
		this.alamatKaryawan = alamatKaryawan;
	}
	public String getStatusKaryawan() {
		return statusKaryawan;
	}
	public void setStatusKaryawan(String statusKaryawan) {
		this.statusKaryawan = statusKaryawan;
	}
	/*@ManyToOne(fetch=FetchType.EAGER)
	@JoinTable
	public RoleModel getRoleKaryawan() {
		return roleKaryawan;
	}
	public void setRoleKaryawan(RoleModel roleKaryawan) {
		this.roleKaryawan = roleKaryawan;
	}*/
	
}
