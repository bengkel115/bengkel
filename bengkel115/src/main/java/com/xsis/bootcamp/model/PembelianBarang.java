package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="pembelian_barang")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="idPembelian")
public class PembelianBarang {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="MST_Pembelian")
	@TableGenerator(name="MST_Pembelian",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="MST_Pembelian", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	private int ID;
	@Column(name="KODE")
	private String kodePembelian;
	@Column(name="ID_PEMASOK")
	private int idPemasok;
	@Column(name="TANGGAL")
	private Date tanggal;
	@Column(name="HARGA")
	private String hargaTotal;
	@Column(name="nama_karyawan")
	private String namaKaryawan;
	
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn(name="ID_PEMASOK", nullable= false, insertable = false, updatable = false)
	private PemasokModel pemasokModel;
	
	@OneToMany(mappedBy="pembelian")
	private List<DetailPembelian> detail;
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getKodePembelian() {
		return kodePembelian;
	}

	public void setKodePembelian(String kodePembelian) {
		this.kodePembelian = kodePembelian;
	}


	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public String getHargaTotal() {
		return hargaTotal;
	}

	public void setHargaTotal(String hargaTotal) {
		this.hargaTotal = hargaTotal;
	}

	public PemasokModel getPemasokModel() {
		return pemasokModel;
	}

	public void setPemasokModel(PemasokModel pemasokModel) {
		this.pemasokModel = pemasokModel;
	}

	public String getNamaKaryawan() {
		return namaKaryawan;
	}

	public void setNamaKaryawan(String namaKaryawan) {
		this.namaKaryawan = namaKaryawan;
	}

	public List<DetailPembelian> getDetail() {
		return detail;
	}

	public void setDetail(List<DetailPembelian> detail) {
		this.detail = detail;
	}

	public int getIdPemasok() {
		return idPemasok;
	}

	public void setIdPemasok(int idPemasok) {
		this.idPemasok = idPemasok;
	}
	
	
}
