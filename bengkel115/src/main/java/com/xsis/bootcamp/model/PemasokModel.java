package com.xsis.bootcamp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PEMASOK")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="idPemasok")
public class PemasokModel {
	private int id;
	private String kodePemasok;
	private String namaPemasok;
	private String teleponPemasok;
	private String alamatPemasok;
	private String statusPemasok;
	private List<PembelianBarang> pembelianBarang;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="PEMASOK")
	@TableGenerator(name="PEMASOK",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="PEMASOK", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="KODE")
	public String getKodePemasok() {
		return kodePemasok;
	}
	public void setKodePemasok(String kodePemasok) {
		this.kodePemasok = kodePemasok;
	}
	
	@Column(name="NAMA")
	public String getNamaPemasok() {
		return namaPemasok;
	}
	public void setNamaPemasok(String namaPemasok) {
		this.namaPemasok = namaPemasok;
	}
	
	@Column(name="TELEPON")
	public String getTeleponPemasok() {
		return teleponPemasok;
	}
	public void setTeleponPemasok(String teleponPemasok) {
		this.teleponPemasok = teleponPemasok;
	}
	
	@Column(name="ALAMAT")
	public String getAlamatPemasok() {
		return alamatPemasok;
	}
	public void setAlamatPemasok(String alamatPemasok) {
		this.alamatPemasok = alamatPemasok;
	}
	
	@Column(name="STATUS")
	public String getStatusPemasok() {
		return statusPemasok;
	}
	public void setStatusPemasok(String statusPemasok) {
		this.statusPemasok = statusPemasok;
	}
	
	@OneToMany(mappedBy="pemasokModel", fetch = FetchType.EAGER)
	@JsonManagedReference
	public List<PembelianBarang> getPembelianBarang() {
		return pembelianBarang;
	}
	public void setPembelianBarang(List<PembelianBarang> pembelianBarang) {
		this.pembelianBarang = pembelianBarang;
	}
}
