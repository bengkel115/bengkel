package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "KATEGORI")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "idKategori")
public class KategoriModel {
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "KATEGORI")
	@TableGenerator(name = "KATEGORI", table = "POS_MST_SEQUENCE", pkColumnName = "SEQUENCE_ID", pkColumnValue = "KATEGORI", valueColumnName = "SEQUENCE_VALUE", allocationSize = 1, initialValue = 1)
	private int id;
	@Column(name = "KODE")
	private String kodeKategori;
	@Column(name = "NAMA")
	private String namaKategori;
	@Column(name = "KETERANGAN")
	private String keteranganKategori;
	@Column(name = "BIAYA_SERVICE")
	private String biayaServiceKategori;
	@Column(name = "STATUS")
	private String statusKategori;

	@OneToMany(mappedBy = "kategoriModel", fetch = FetchType.EAGER)
	@JsonManagedReference
	private List<SparepartModel> sparepartModel;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKodeKategori() {
		return kodeKategori;
	}

	public void setKodeKategori(String kodeKategori) {
		this.kodeKategori = kodeKategori;
	}

	public String getNamaKategori() {
		return namaKategori;
	}

	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}

	public String getKeteranganKategori() {
		return keteranganKategori;
	}

	public void setKeteranganKategori(String keteranganKategori) {
		this.keteranganKategori = keteranganKategori;
	}

	public String getBiayaServiceKategori() {
		return biayaServiceKategori;
	}

	public void setBiayaServiceKategori(String biayaServiceKategori) {
		this.biayaServiceKategori = biayaServiceKategori;
	}

	public String getStatusKategori() {
		return statusKategori;
	}

	public void setStatusKategori(String statusKategori) {
		this.statusKategori = statusKategori;
	}

	public List<SparepartModel> getSparepartModel() {
		return sparepartModel;
	}

	public void setSparepartModel(List<SparepartModel> sparepartModel) {
		this.sparepartModel = sparepartModel;
	}
}
