package com.xsis.bootcamp.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="SPAREPART")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="idSparepart")
public class SparepartModel {
	private int id;
	private String kodeSparepart;
	private String namaSparepart;
	private String stokSparepart;
	private String hargaSparepart;
	private String statusSparepart;
	private int idKategori;
	private KategoriModel kategoriModel;
	private DetailPembelian detailPembelian;
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="SPAREPART")
	@TableGenerator(name="SPAREPART",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="SPAREPART", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="KODE")
	public String getKodeSparepart() {
		return kodeSparepart;
	}
	public void setKodeSparepart(String kodeSparepart) {
		this.kodeSparepart = kodeSparepart;
	}
	
	@Column(name="NAMA")
	public String getNamaSparepart() {
		return namaSparepart;
	}
	public void setNamaSparepart(String namaSparepart) {
		this.namaSparepart = namaSparepart;
	}
	
	@Column(name="STATUS")
	public String getStatusSparepart() {
		return statusSparepart;
	}
	public void setStatusSparepart(String statusSparepart) {
		this.statusSparepart = statusSparepart;
	}
	
	@Column(name="STOK")
	public String getStokSparepart() {
		return stokSparepart;
	}
	public void setStokSparepart(String stokSparepart) {
		this.stokSparepart = stokSparepart;
	}

	@Column(name="HARGA")
	public String getHargaSparepart() {
		return hargaSparepart;
	}
	public void setHargaSparepart(String hargaSparepart) {
		this.hargaSparepart = hargaSparepart;
	}
	
	@Column(name="ID_KATEGORI")
	public int getIdKategori() {
		return idKategori;
	}
	public void setIdKategori(int idKategori) {
		this.idKategori = idKategori;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_KATEGORI", nullable= false, insertable = false, updatable = false)	
	public KategoriModel getKategoriModel() {
		return kategoriModel;
	}
	public void setKategoriModel(KategoriModel kategoriModel) {
		this.kategoriModel = kategoriModel;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ID_DETAILPEMBELIAN", insertable = false, updatable = false)	
	public DetailPembelian getDetailPembelian() {
		return detailPembelian;
	}
	public void setDetailPembelian(DetailPembelian detailPembelian) {
		this.detailPembelian = detailPembelian;
	}	
}
