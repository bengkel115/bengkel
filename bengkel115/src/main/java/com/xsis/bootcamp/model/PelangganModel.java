package com.xsis.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="PELANGGAN")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="pelangganId")
public class PelangganModel {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.TABLE,generator="PELANGGAN")
	@TableGenerator(name="PELANGGAN",table="POS_MST_SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="PELANGGAN", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	private int id;
	@Column(name="KODE")
	private String kodePelanggan;
	@Column(name="NAMA")
	private String namaPelanggan;
	@Column(name="TELEPON")
	private String teleponPelanggan;
	@Column(name="ALAMAT")
	private String alamatPelanggan;
	@Column(name="STATUS")
	private String statusPelanggan;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKodePelanggan() {
		return kodePelanggan;
	}
	public void setKodePelanggan(String kodePelanggan) {
		this.kodePelanggan = kodePelanggan;
	}
	public String getNamaPelanggan() {
		return namaPelanggan;
	}
	public void setNamaPelanggan(String namaPelanggan) {
		this.namaPelanggan = namaPelanggan;
	}
	public String getTeleponPelanggan() {
		return teleponPelanggan;
	}
	public void setTeleponPelanggan(String teleponPelanggan) {
		this.teleponPelanggan = teleponPelanggan;
	}
	public String getAlamatPelanggan() {
		return alamatPelanggan;
	}
	public void setAlamatPelanggan(String alamatPelanggan) {
		this.alamatPelanggan = alamatPelanggan;
	}
	public String getStatusPelanggan() {
		return statusPelanggan;
	}
	public void setStatusPelanggan(String statusPelanggan) {
		this.statusPelanggan = statusPelanggan;
	}
}
