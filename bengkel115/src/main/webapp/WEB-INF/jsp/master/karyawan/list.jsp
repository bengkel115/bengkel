<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${karyawanList.size() > 0}">
		<c:forEach items="${karyawanList}" var="karyawan">

			<tr>
				<td>${karyawan.kodeKaryawan}</td>
				<td>${karyawan.namaKaryawan}</td>
				<td>${karyawan.statusKaryawan}</td>
				<td>
				<button type="button" class="btn btn-success btn-xs btn-info" value="${karyawan.id}"><i class="fa fa-info-circle"></i></button>
				<button type="button" class="btn btn-success btn-xs btn-edit" value="${karyawan.id}"><i class="fa fa-edit"></i></button>
				<button type="button" class="btn btn-danger btn-xs btn-delete" value="${karyawan.id}"><i class="fa fa-trash"></i></button>
				</td>
			</tr>

		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="4" align="center">No Data Available</td>
		</tr>
	</c:otherwise>
</c:choose>
