
<div class="box box-primary">
	<div class="box-header with-border">
		<h4 class="text-light-blue">Ubah Karyawan</h4>
	</div>
	<br />
	<form id="form-karyawan" method="post" class="form-horizontal">
		<input type="hidden" id="action" name="action" value="update" /> 
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" id="id" name="id" value="${karyawan.id}" />
		<div class="box-body">
			<table class="add">
				<tr>
					<td>Kode</td>
					<td colspan="2"><input type="text" class="form-control"
						name="kodeKaryawan" id="kodeKaryawan" value="${karyawan.kodeKaryawan}" readonly="readonly"/></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td colspan="2"><input type="text" class="form-control"
						name="namaKaryawan" id="namaKaryawan" value="${karyawan.namaKaryawan}" /></td>
				</tr>
				<tr>
					<td>Username</td>
					<td colspan="2"><input type="text" class="form-control"
						name="username" id="username" value="${karyawan.username}" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td colspan="2"><input type="password" class="form-control"
						name="password" id="password" value="${karyawan.password}" /></td>
				</tr>
				<tr>
					<td>Telepon</td>
					<td colspan="2"><input type="text" class="form-control"
						name="teleponKaryawan" id="teleponKaryawan" value="${karyawan.teleponKaryawan}" /></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td colspan="2"><textarea rows="5" cols="10" name="alamatKaryawan"
							id="alamatKaryawan" class="form-control" placeholder="Alamat...">${karyawan.alamatKaryawan}</textarea></td>
				</tr>
				<tr>
					<td>Status</td>
					<td colspan="2"><select class="form-control" name="statusKaryawan"
						id="statusKaryawan">
							<option value="Aktif">Aktif</option>
							<option value="Tidak Aktif">Tidak Aktif</option>
					</select></td>
				</tr>
				<tr>
					<td>
						<button class="btn btn-primary" type="submit">Submit</button>
					</td>
				</tr>
			</table>

		</div>
	</form>
</div>