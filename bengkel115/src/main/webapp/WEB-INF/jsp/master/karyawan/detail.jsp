<div class="box box-primary">
	<div class="box-body box-profile">
		<ul class="list-group list-group-unbordered">
			<li class="list-group-item"><b>Kode</b> <a
				class="pull-right">${karyawan.kodeKaryawan}</a></li>
			<li class="list-group-item"><b>Nama</b> <a
				class="pull-right">${karyawan.namaKaryawan}</a></li>
			<li class="list-group-item"><b>Username</b> <a
				class="pull-right">${karyawan.username}</a></li>
			<li class="list-group-item"><b>Password</b> <a 
				class="pull-right">${karyawan.password}</a></li>
			<li class="list-group-item"><b>Telepon</b> <a
				class="pull-right">${karyawan.teleponKaryawan}</a></li>
			<li class="list-group-item"><b>Alamat</b> <a
				class="pull-right">${karyawan.alamatKaryawan}</a></li>
			<li class="list-group-item"><b>Status</b> <a
				class="pull-right">${karyawan.statusKaryawan}</a></li>
		</ul>
	</div>
	<!-- /.box-body -->
</div>