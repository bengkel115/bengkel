
<div class="box box-primary">
	<div class="box-header with-border">
		<h4 class="text-light-blue">Tambah Karyawan</h4>
	</div>
	<br />
	<form id="form-karyawan" method="post" class="form-horizontal">
		<input type="hidden" id="action" name="action" value="insert" /> 
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<div class="box-body">
			<table class="add">
				<tr>
					<td>Kode</td>
					<td colspan="2"><input type="text" class="form-control"
						name="kodeKaryawan" id="kodeKaryawan" placeholder="Kode..."></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td colspan="2"><input type="text" class="form-control"
						name="namaKaryawan" id="namaKaryawan" placeholder="Nama..."></td>
				</tr>
				<tr>
					<td>Username</td>
					<td colspan="2"><input type="text" class="form-control"
						name="username" id="username" placeholder="Username..."></td>
				</tr>
				<tr>
					<td>Password</td>
					<td colspan="2"><input type="password" class="form-control"
						name="password" id="password" placeholder="Password..."></td>
				</tr>
				<tr>
					<td>Telepon</td>
					<td colspan="2"><input type="text" class="form-control"
						name="teleponKaryawan" id="teleponKaryawan" placeholder="Telepon..."></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td colspan="2"><textarea rows="5" cols="10" name="alamatKaryawan"
							id="alamatKaryawan" class="form-control" placeholder="Alamat..."></textarea></td>
				</tr>
				<tr>
					<td>Status</td>
					<td colspan="2"><input type="text" class="form-control"
						name="statusKaryawan" id="statusKaryawan" value="Aktif" readonly></td>
				</tr>
				<tr>
					<td>
						<button class="btn btn-primary" type="submit">Submit</button>
					</td>
				</tr>
			</table>

		</div>
	</form>
</div>