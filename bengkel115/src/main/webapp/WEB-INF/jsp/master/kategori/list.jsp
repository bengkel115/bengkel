<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

			<c:choose>
				<c:when test="${kategoriList.size()>0}">
					<c:forEach items="${kategoriList}" var="kategori">

						<tr>
							<td>${kategori.kodeKategori}</td>
							<td>${kategori.namaKategori}</td>
							<td>${kategori.statusKategori}</td>
							<td>
							<button type="button" class="btn btn-success btn-xs btn-info" value="${kategori.id}"><i class="fa  fa-info-circle"></i></button>
							<button type="button" class="btn btn-success btn-xs btn-edit" value="${kategori.id}"><i class="fa fa-edit"></i></button>
							<button type="button" class="btn btn-danger btn-xs btn-delete" value="${kategori.id}"><i class="fa fa-trash"></i></button>
							
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="6">No Data</td>
					</tr>
				</c:otherwise>
			</c:choose>
