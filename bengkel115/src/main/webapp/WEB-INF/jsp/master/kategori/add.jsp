<form id="form-kategori" xmethod="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="insert"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="text" id="kodeKategori" name="kodeKategori"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" id="namaKategori" name="namaKategori"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td><textarea rows="5" cols="20" id="keteranganKategori"
							name="keteranganKategori" class="form-control"></textarea></td>
				</tr>
				<tr>
					<td>Biaya Service</td>
					<td><input type="text" id="biayaServiceKategori"
						name="biayaServiceKategori" class="form-control" /></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" id="statusKategori" name="statusKategori"
						class="form-control" /></td>
				</tr>
				<tr>
					<td></td>
					<td><button type="submit" class="btn btn-info ">Submit</button></td>
				</tr>
			</tbody>
		</table>

	</div>
</form>
