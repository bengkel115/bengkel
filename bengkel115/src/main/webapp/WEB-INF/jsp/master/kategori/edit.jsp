<form id="form-kategori" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="update" />
	<input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}" />
	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="hidden" id="kodeKategori" name="kodeKategori"
						value="${item.kodeKategori}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" id="namaKategori" name="namaKategori"
						value="${item.namaKategori}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td><textarea rows="5" cols="20" id="keteranganKategori" name="keteranganKategori"
							class="form-control" value="${item.keteranganKategori}"></textarea></td>
				</tr>
				<tr>
					<td>Biaya Service</td>
					<td><input type="text" id="biayaServiceKategori" name="biayaServiceKategori"
						value="${item.biayaServiceKategori}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" id="statusKategori" name="statusKategori"
						value="${item.statusKategori}" class="form-control" /></td>
				</tr>
				<tr>
					<td><button type="submit" class="btn btn-info ">Submit</button></td>
				</tr>
			</tbody>
		</table>
</form>
</div>
