<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="form-sparepart" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="insert"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="text" id="kodeSparepart" name="kodeSparepart"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" id="namaSparepart" name="namaSparepart"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama Kategori</td>
					<td><select id="idKategori" name="idKategori" class="form-control">
							<c:forEach items="${kategoriList}" var="kategori">
								<option value="${kategori.id}" >${kategori.namaKategori}</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Stok</td>
					<td><input type="text" id="stokSparepart"
						name="stokSparepart" class="form-control" /></td>
				</tr>
				<tr>
					<td>Harga</td>
					<td><input type="text" id="hargaSparepart"
						name="hargaSparepart" class="form-control" /></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" id="statusSparepart" name="statusSparepart"
						class="form-control" /></td>
				</tr>
				<tr>
					<td></td>
					<td><button type="submit" class="btn btn-info ">Submit</button></td>
				</tr>
			</tbody>
		</table>

	</div>
</form>
