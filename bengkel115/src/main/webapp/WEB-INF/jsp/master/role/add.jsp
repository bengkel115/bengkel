
<div class="box box-primary">
	<div class="box-header with-border">
		<h4 class="text-light-blue">Tambah Role</h4>
	</div>
	<br />
	<form id="form-role" method="post" class="form-horizontal">
		<input type="hidden" id="action" name="action" value="insert" /> 
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<div class="box-body">
			<table class="add">
				<tr>
					<td>Kode</td>
					<td colspan="2"><input type="text" class="form-control"
						name="kodeRole" id="kodeRole" placeholder="Kode..."></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td colspan="2"><input type="text" class="form-control"
						name="namaRole" id="namaRole" placeholder="Nama..."></td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td colspan="2"><select class="form-control" name="keteranganRole"
						id="keteranganRole">
							<option value="Admin">Admin</option>
							<option value="Back Office">Back Office</option>
							<option value="Kasir">Kasir</option>
							<option value="Teknisi">Teknisi</option>
					</select></td>
				</tr>
				<tr>
					<td>Status</td>
					<td colspan="2"><input type="text" class="form-control"
						name="statusRole" id="statusRole" value="Aktif" readonly></td>
				</tr>
				<tr>
					<td>
						<button class="btn btn-primary" type="submit">Submit</button>
					</td>
				</tr>
			</table>

		</div>
	</form>
</div>