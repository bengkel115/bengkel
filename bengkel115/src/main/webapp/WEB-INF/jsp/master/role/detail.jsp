<div class="box box-primary">
	<div class="box-body box-profile">
		<ul class="list-group list-group-unbordered">
			<li class="list-group-item"><b>Kode</b> <a
				class="pull-right">${role.kodeRole}</a></li>
			<li class="list-group-item"><b>Nama</b> <a
				class="pull-right">${role.namaRole}</a></li>
			<li class="list-group-item"><b>Keterangan</b> <a
				class="pull-right">${role.keteranganRole}</a></li>
			<li class="list-group-item"><b>Status</b> <a
				class="pull-right">${role.statusRole}</a></li>
		</ul>
	</div>
	<!-- /.box-body -->
</div>