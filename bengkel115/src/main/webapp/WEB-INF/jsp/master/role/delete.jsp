<p>
	Anda Yakin akan Menghapus Data Role ${role.namaRole}
</p>
<form id="form-role" method="post" class="form-horizontal">

	<input type="hidden" id="action" name="action" value="delete"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${role.id}" />
	
	<div class="modal-footer">
		<button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
		<button type="submit" class="btn btn-danger"><i class="fa fa-trash" onclick="ganti()"></i> Hapus</button>
	</div>
</form>

<script type="text/javascript">
	function ganti() {
		$('#statusKaryawan').val('Tidak Aktif');
	}



</script>