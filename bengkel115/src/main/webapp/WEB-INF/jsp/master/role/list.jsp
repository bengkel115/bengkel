<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${roleList.size() > 0}">
		<c:forEach items="${roleList}" var="role">

			<tr>
				<td>${role.kodeRole}</td>
				<td>${role.namaRole}</td>
				<td>${role.statusRole}</td>
				<td>${role.keteranganRole}</td>
				<td>
				<button type="button" class="btn btn-success btn-xs btn-info" value="${role.id}"><i class="fa fa-info-circle"></i></button>
				<button type="button" class="btn btn-success btn-xs btn-edit" value="${role.id}"><i class="fa fa-edit"></i></button>
				<button type="button" class="btn btn-danger btn-xs btn-delete" value="${role.id}"><i class="fa fa-trash"></i></button>
				</td>
			</tr>

		</c:forEach>
	</c:when>
	<c:otherwise>
		<tr>
			<td colspan="5" align="center">No Data Available</td>
		</tr>
	</c:otherwise>
</c:choose>
