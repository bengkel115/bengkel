<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

			<c:choose>
				<c:when test="${pelangganList.size()>0}">
					<c:forEach items="${pelangganList}" var="pelanggan">

						<tr>
							<td>${pelanggan.kodePelanggan}</td>
							<td>${pelanggan.namaPelanggan}</td>
							<td>${pelanggan.statusPelanggan}</td>
							<td>
							<button type="button" class="btn btn-success btn-xs btn-info" value="${pelanggan.id}"><i class="fa  fa-info-circle"></i></button>
							<button type="button" class="btn btn-success btn-xs btn-edit" value="${pelanggan.id}"><i class="fa fa-edit"></i></button>
							<button type="button" class="btn btn-danger btn-xs btn-delete" value="${pelanggan.id}"><i class="fa fa-trash"></i></button>
							
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="6">No Data</td>
					</tr>
				</c:otherwise>
			</c:choose>
