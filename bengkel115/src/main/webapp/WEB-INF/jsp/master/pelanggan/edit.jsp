<form id="form-pelanggan" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="update" />
	<input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}" />
	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="hidden" id="kodePelanggan" name="kodePelanggan"
						value="${item.kodePelanggan}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" id="namaPelanggan" name="namaPelanggan"
						value="${item.namaPelanggan}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Telepon</td>
					<td><input type="text" id="teleponPelanggan" name="teleponPelanggan"
						value="${item.teleponPelanggan}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><textarea rows="5" cols="20" id="alamatPelanggan" name="alamatPelanggan"
							class="form-control" value="${item.alamatPelanggan}"></textarea></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" id="statusPelanggan" name="statusPelanggan"
						value="${item.statusPelanggan}" class="form-control" /></td>
				</tr>
				<tr>
					<td><button type="submit" class="btn btn-info ">Submit</button></td>
				</tr>
			</tbody>
		</table>
</form>
</div>
