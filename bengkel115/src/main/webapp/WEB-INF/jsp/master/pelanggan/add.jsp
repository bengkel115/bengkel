<form id="form-pelanggan" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="insert"/>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	
	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="text" id="kodePelanggan" name="kodePelanggan"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" id="namaPelanggan" name="namaPelanggan"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Telepon</td>
					<td><input type="text" id="teleponPelanggan"
						name="teleponPelanggan" class="form-control" /></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><textarea rows="5" cols="20" id="alamatPelanggan"
							name="alamatPelanggan" class="form-control"></textarea></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" id="statusPelanggan" name="statusPelanggan"
						class="form-control" /></td>
				</tr>
				<tr>
					<td></td>
					<td><button type="submit" class="btn btn-info ">Submit</button></td>
				</tr>
			</tbody>
		</table>

	</div>
</form>
