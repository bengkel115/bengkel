<form id="form-pemasok" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="update" />
	<input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<input type="hidden" id="id" name="id" value="${item.id}" />
	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="hidden" id="kodePemasok" name="kodePemasok"
						value="${item.kodePemasok}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td><input type="text" id="namaPemasok" name="namaPemasok"
						value="${item.namaPemasok}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Telepon</td>
					<td><input type="text" id="teleponPemasok" name="teleponPemasok"
						value="${item.teleponPemasok}" class="form-control" /></td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td><textarea rows="5" cols="20" id="alamatPemasok" name="alamatPemasok"
							class="form-control" value="${item.alamatPemasok}"></textarea></td>
				</tr>
				<tr>
					<td>Status</td>
					<td><input type="text" id="statusPemasok" name="statusPemasok"
						value="${item.statusPemasok}" class="form-control" /></td>
				</tr>
				<tr>
					<td><button type="submit" class="btn btn-info ">Submit</button></td>
				</tr>
			</tbody>
		</table>
</form>
</div>
