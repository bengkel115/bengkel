<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="form-pembelian" method="post" class="form-horizontal">
	<input type="hidden" id="action" name="action" value="insert" /> <input
		type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

	<div class="box-body">
		<table>
			<tbody>
				<tr>
					<td>Kode</td>
					<td><input type="text" id="kodePembelian" name="kodePembelian"
						class="form-control" /></td>
				</tr>

				<tr>
					<td>Nama Supplier</td>
					<td><select id="pemasokmodel_id" name="pemasokmodel_id" class="form-control">
							<c:forEach items="${pemasokList}" var="pemasok">
								<option value="${pemasok.id}" >${pemasok.namaPemasok}</option>
							</c:forEach>
					</select></td>
				</tr>

				<tr>
					<td>Tanggal</td>
					<td><input type="text" id="tanggal"
						name="tanggal" class="form-control" /></td>
				</tr>
				<tr>
					<td>Harga</td>
					<td><input type="text" id="hargaTotal" name="hargaTotal"
						class="form-control" /></td>
				</tr>
				<tr>
					<td>Nama Karyawan</td>
					<td><input type="text" id="namaKaryawan" name="namaKaryawan"
						class="form-control" /></textarea></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-info">
				<div class="box-header">
					<h3 class="box-title">Data Item Variant</h3>
					<div class="box-tools">
						<button type="button" id="btn-add-pembelian"
							class="btn btn-primary btn-sm">
							<i class="fa fa-plus"></i>
						</button>
					</div>
				</div>
				<div class="box-body">
					<table class="table table-considered">
						<thead>
							<tr>
								<td class="col-md-4">Sparepart</td>
								<td class="col-md-2">Harga Item</td>
								<td class="col-md-2">Jumlah Item</td>
								<td class="col-md-2">Beginning Stock</td>
							</tr>
						</thead>
						<tbody id="list-variant">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-info ">Submit</button>
	</div>
</form>
