<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- LIST EMPLOYEE  -->

<c:choose>
	<c:when test="${pembelianList.size()>=0}">
		<c:forEach items="${pembelianList}" var="pembelian">
			<tr>
				<td>${pembelian.kodePembelian}</td>
				<td>${pembelian.nama}</td>
				<td>${pembelian.tanggal}</td>
				<td><button type="button"
						class="btn btn-success btn-xs btn-info" value="${pembelian.ID}">
						<i class="fa fa-trash"></i>
					</button>
					<button type="button" class="btn btn-success btn-xs btn-edit"
						value="${pembelian.ID}">
						<i class="fa fa-edit"></i>
					</button>
					<button type="button" class="btn btn-danger btn-xs btn-delete"
						value="${pembelian.ID}">
						<i class="fa fa-trash"></i>
					</button></td>
			</tr>
		</c:forEach>
	</c:when>

	<c:otherwise>
		<tr>
			<td colspan="4">No Data</td>
		</tr>
	</c:otherwise>
</c:choose>
