<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<form id="form-pembelian" class="form-horizontal">
	<input type="hidden" id="var-action" name="var-action" value="insert">
	<input type="hidden" id="id" name="id">

	<div class="form-group">
		<label class="control-label col-md-2" for="variantName">Sparepart</label>
		<div class="col-md-10">
			<select id="idSparepart" name="idSparepart"
				class="form-control">
				<c:forEach items="${sparepartList}" var="sparepart">
					<option value="${sparepart.id}">${sparepart.namaSparepart}</option>
				</c:forEach>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-4" for="unitPrice">Harga
					Item</label>
				<div class="col-md-8">
					<input type="text" id="hargaItem" name="hargaItem"
						class="form-control" />
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-md-4" for="sku">Jumlah Item</label>
				<div class="col-md-8">
					<input type="text" id="jumlahItem" name="jumlahItem"
						class="form-control" />
				</div>
			</div>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-default pull-left"
			data-dismiss="modal">
			<i class="fa fa-close"></i> Close
		</button>
		<button type="button" id="btn-add-variant" class="btn btn-primary">
			<i class="fa fa-plus"></i> Add
		</button>
	</div>
</form>