<% request.setAttribute("contextName", request.getContextPath()); %>
<div id="modal-form" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 id="modal-title">Form Pembelian</h4>
			</div>
			<div class="modal-body">
				
			</div>
		</div>
	</div>
</div>

<div id="modal-variant" class="modal modal-primary" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 id="modal-title" class="modal-title">Primary Modal</h4>
			</div>
			<div class="modal-body">
				<%@include file="pembelian/addDetail.jsp" %>
			</div>
		</div>
	</div>
</div>

<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Data Pembelian</h3>
	</div>
	<div class="box-header">
		<div class="input-group-btn">
               	<button type="button" id="btn-add" class="btn btn-primary"><i>Add Pembelian</i></button>
        </div>
		<div class="box-tools">
           <div class="input-group input-group-sm" style="width: 250px;">
             <input type="text" id="txt-search" class="form-control pull-right" placeholder="Search">
             <div class="input-group-btn">
               	<button type="button" id="btn-search" class="btn btn-default"><i class="fa fa-search"></i></button>
             </div>
           </div>
         </div>
	</div>
	<div class="box-body">
		<table class="table table-considered">
			<thead>
				<tr>
					<td>Kode Pembelian</td>
					<td>Nama Supplier</td>
					<td>Tanggal</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody id="list-data">
			
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	function loadData(){
		$.ajax({
			url:'${contextName}/transaksi/pembelian/list',
			dataType:'html',
			type:'get',
			success:function(result){
				$("#list-data").html(result);
			}
		});
	}
	
	$(document).ready(function(){
		// load data first display
		loadData();
		
		$("#btn-add").click(function(){
			$.ajax({
				url:'${contextName}/transaksi/pembelian/add',
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Menambah Pembelian Baru");
					$("#modal-form").removeClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		$("#btn-add-pembelian").click(function(){
			$.ajax({
				url:'${contextName}/transaksi/pembelian/addDetail',
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Menambah Pembelian Baru");
					$("#modal-form").removeClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		$("#list-data").on('click','.btn-info',function(){
			var id = $(this).val();
			$.ajax({
				url:'${contextName}/transaksi/pembelian/detail',
				data:{'id':id},
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Melihat Data Pembelian");
					$("#modal-form").removeClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		$("#list-data").on('click','.btn-edit',function(){
			var id = $(this).val();
			$.ajax({
				url:'${contextName}/transaksi/pembelian/edit',
				data:{'id':id},
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Mengubah Data Pembelian");
					$("#modal-form").removeClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		$("#list-data").on('click','.btn-delete',function(){
			var id = $(this).val();
			$.ajax({
				url:'${contextName}/transaksi/pembelian/delete',
				data:{'id':id},
				type:'get',
				dataType:'html',
				success:function(result){
					$("#modal-form").find(".modal-body").html(result);
					$("#modal-title").html("Menghapus Data Pembelian");
					$("#modal-form").addClass("modal-danger");
					$("#modal-form").modal("show");
				}
			});
		});
		
		$("#btn-search").click(function(){
			var key = $("#txt-search").val();
			$.ajax({
				url:'${contextName}/transaksi/pembelian/search',
				data:{'key':key},
				dataType:'html',
				type:'get',
				success:function(result){
					$("#list-data").html(result);
				}
			});
		});
		
		// simpan data dari form
		$("#modal-form").on("submit","#form-pembelian", function(){
			$.ajax({
				url:'${contextName}/transaksi/pembelian/save.json',
				type:'post',
				data:$(this).serialize(),
				success:function(result){
					if(result.message=="success"){
						$("#modal-form").modal("hide");
						loadData();
					}
				}
			});				
			return false;
		});
		
		$("#modal-form").on("click","#btn-add-pembelian",function(){
			$("#form-variant").trigger("reset");
			$("#var-action").val("insert");
			$("#modal-variant").modal("show");
		});
		
		// add variant
		$('#btn-add-pembelian').click(function(){
			
			var variantName = $("#variantName").val();
			var unitPrice = $('#unitPrice').val();
			var sku = $("#sku").val();
			var beginningStock = $("#beginningStock").val();
			var alertAt =$("#alertAt").val();
			var action = $("#var-action").val();
			
			// hide modal varian
			$("#modal-variant").modal("hide");
		});
	});
</script>