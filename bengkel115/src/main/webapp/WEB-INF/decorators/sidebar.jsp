<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${contextName}/assets/dist/img/user2-160x160.jpg"
					class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>${username}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>

		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Master Data</span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/master/karyawan.html"
						class="menu-item"> <i class="fa fa-circle-o"></i>Karyawan
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/master/role.html"
						class="menu item"> <i class="fa fa-circle-o"></i>Role
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/master/kategori.html"
						class="menu-item"> <i class="fa fa-circle-o"></i>Kategori
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/master/sparepart.html"
						class="menu item"> <i class="fa fa-circle-o"></i>Sparepart
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/master/pemasok.html"
						class="menu-item"> <i class="fa fa-circle-o"></i>Pemasok
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/master/pelanggan.html"
						class="menu item"> <i class="fa fa-circle-o"></i>Pelanggan
					</a></li>
				</ul>
				</li>
			<li class="treeview"><a href="#"> <i class="fa fa-laptop"></i>
					<span>Transaksi</span> <span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${contextName}/transaksi/txpembelian.html"
						class="menu-item"> <i class="fa fa-circle-o"></i>Transaksi Pembelian
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/transaksi/txpejualan.html"
						class="menu item"> <i class="fa fa-circle-o"></i>Transaksi Penjualan
					</a></li>
				</ul>
				<ul class="treeview-menu">
					<li><a href="${contextName}/transaksi/txservice.html"
						class="menu-item"> <i class="fa fa-circle-o"></i>Transaksi Service
					</a></li>
				</ul>				
				</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>